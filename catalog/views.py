from django.shortcuts import render
from postdetails.models import Post, PostLocation, PostCategory
from .models import Visitor,Saran
from .forms import Input_saran

# Create your views here.

def filter(request):
    db = Post.objects.all()
    nama_produk = request.GET.get("cari_produk")
    lokasi = request.GET.get("lokasi")
    kategori = request.GET.get("kategori")

    if is_valid_queryparam(nama_produk):
        if(db.filter(description__icontains = nama_produk)!= None ):
            db = db.filter(description__icontains = nama_produk)
                
    if is_valid_queryparam(lokasi):
        db = db.filter(location__location = lokasi)
    if is_valid_queryparam(kategori):
        db = db.filter(category__category = kategori)

    return db

def katalog(request):
    filtering = filter(request)
    visit = Visitor.objects.all()
    lokasi = PostLocation.objects.all()
    kategori = PostCategory.objects.all()
    count = request.session.get('count',0)
    newcount = count+1
    request.session['count'] = newcount
    visit.visit = newcount
    
    form = Input_saran()
    if request.method == 'POST':
        form = Input_saran(request.POST)
        if form.is_valid() and request.POST != '':
            saran = form.save(commit=False)
            saran.user = request.user
            saran.save()

    context ={
        'post' : filtering,
        'visit': visit.visit,
        'lokasi' : lokasi,
        'kategori' : kategori,
        'form' : form
    }
    return render(request,'catalog/katalog.html', context)

def is_valid_queryparam(param):
    return param != '' and param is not None

def saran(request):
    saran = Saran.objects.all()
    content = {"saran" : saran }
    return render(request,'catalog/saran_katalog.html',content)
