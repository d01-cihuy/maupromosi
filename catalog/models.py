from django.db import models

class Saran(models.Model):
    saran = models.CharField(max_length=100)

class Visitor(models.Model):
    visit = models.IntegerField(default=0, null=True, blank=True)