from django.urls import re_path, path
from .views import *

# Website urls
urlpatterns = [
    #re_path(r'^$', index, name = 'index'),
    path('', homepage, name='homepage'),
    path('about-us/', about, name='about'),
    path('feedback/', list_feedback, name='feedback')
]
