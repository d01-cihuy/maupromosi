from django import forms
from .models import Feedback


class FormFeedback(forms.ModelForm):

    message = forms.CharField(max_length=300, required=False, widget=forms.TextInput(
        attrs={
            'class': 'form-control mx-auto msg'
        },
    ))

    class Meta:
        model = Feedback
        fields = ('message',)
