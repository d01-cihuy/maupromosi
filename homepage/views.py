from django.shortcuts import render
from .forms import FormFeedback
from .models import Feedback

# Create your views here.
def index(request):
    return render(request, 'base.html', {})

def homepage(request):
    if request.method == 'POST':
        form = FormFeedback(request.POST)
        if form.is_valid() and request.POST != '':
            form.save()
            
    form = FormFeedback()
    return render(request, 'homepage/index.html',{'form' : form})

def about(request):
    return render(request, 'homepage/about.html')

def list_feedback(request):
    message = Feedback.objects.all()
    response = {'feedback': message}
    return render(request, 'homepage/feedback.html', response)