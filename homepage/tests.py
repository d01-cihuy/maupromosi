from django.test import TestCase, Client
from django.urls import resolve
from .models import Feedback
from .views import *
from .forms import FormFeedback

# Create your tests here.
class TestHome(TestCase):
    
    #Test url
    def test_url_homepage(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_url_about(self):
        response = Client().get('/about-us/')
        self.assertEquals(response.status_code, 200)
    
    def test_url_feedback(self):
        response = Client().get('/feedback/')
        self.assertEquals(response.status_code, 200)

    #Test templates
    def test_using_homepage_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage/index.html')
    
    def test_using_about_template(self):
        response = Client().get('/about-us/')
        self.assertTemplateUsed(response, 'homepage/about.html')
    
    def test_using_feedback_template(self):
        response = Client().get('/feedback/')
        self.assertTemplateUsed(response, 'homepage/feedback.html')
    
    #Test Views
    def test_homepage_using_correct_views(self):
        view = resolve('/')
        self.assertEqual(view.func, homepage)
    
    def test_about_using_correct_views(self):
        view = resolve('/about-us/')
        self.assertEqual(view.func, about)
    
    def test_feedback_using_correct_views(self):
        view = resolve('/feedback/')
        self.assertEqual(view.func, list_feedback)

    #Test Model
    def test_model_Feedback(self):
        fb = Feedback.objects.create(message='FEEDBACK')
        jumlah= Feedback.objects.all().count()
        self.assertEqual(jumlah,1)
    
    def test_halaman_pakai_index_template_setelah_post(self):
        fb = Feedback.objects.create(message="semangat")
        fb.save()
        response = Client().post('/',{'message':'semangat'})
        self.assertEqual(response.status_code, 200)
    
    #Test form
    def test_form_feedback(self):
        form = FormFeedback()
        form_data = {'message':'INI FEEDBACK'}
        form = FormFeedback(data=form_data)
        self.assertTrue(form.is_valid())
