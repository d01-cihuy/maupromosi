from django import template

register = template.Library()

@register.filter
def splitFilter(s, t):
    return s.split(t)
