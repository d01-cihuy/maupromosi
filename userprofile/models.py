from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin, User
from django.db import models
from django.utils import timezone
from gdstorage.storage import GoogleDriveStorage

gd_storage = GoogleDriveStorage()

class UserManager(BaseUserManager):

    def _create_user(self, username, password, is_superuser, is_staff, name,
                    email, instagram, picture, bio, **extra_fields):
        if not username:
            raise ValueError('User must have a valid username')
        now = timezone.now()
        email = self.normalize_email(email)
        user = self.model(
            username=username,
            email = email,
            name = name,
            instagram = instagram,
            picture = picture,
            bio = bio,
            last_login = now,
            date_joined = now,
            is_superuser = is_superuser,
            is_staff = is_staff,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user
    
    def create_user(self, username, password, **extra_fields):
        return self._create_user(username, password, False, False, **extra_fields)

    def create_superuser(self, username, password, **extra_fields):
        user = self._create_user(username, password, True, True, **extra_fields)
        user.save(using=self._db)
        return user

class User(AbstractBaseUser, PermissionsMixin) :
    username = models.CharField(max_length=50, unique=True)
    email = models.EmailField(max_length=255)
    name = models.CharField(max_length=100)
    instagram = models.URLField(max_length=255)
    picture = models.ImageField(upload_to='maupromosi_v1/', storage=gd_storage, blank=True)
    bio = models.TextField(blank=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    date_joined = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(null=True, blank=True)

    USERNAME_FIELD = 'username'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['email', 'name', 'instagram', 'picture', 'bio']

    objects = UserManager()

    def get_absolute_url(self):
        return "/users/%i" % (self.pk)
