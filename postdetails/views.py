from django.shortcuts import render, redirect
from .models import Post, PostReview
from .forms import *

status_code_dict = {
    'NOTHING' : (0, ''),
    'POST_ADD_SUCCESS' : (1, 'Post added successfully'),
    'POST_ADD_FAILED' : (2, 'Failed to add post'),
    'POST_DELETE_SUCCESS' : (3, 'Post deleted successfully'),
    'POST_DELETE_FAILED' : (4, 'Failed to delete post'),
    'REVIEW_ADD_SUCCESS' : (5, 'Review added successfully'),
    'REVIEW_ADD_FAILED' : (6, 'Failed to add review'),
    'REVIEW_DELETE_SUCCESS' : (7, 'Review deleted successfully'),
    'REVIEW_DELETE_FAILED' : (8, 'Failed to delete review'),
    'REVIEWED' : (9, 'You have reviewed this post, please delete it if you want to create a new review'),
}
    
def posts(request, post_id):
    response = {'status_code':status_code_dict['NOTHING']}
    try:
        post = Post.objects.filter(id=post_id)[0]
        response['post'] = post
        reviews = PostReview.objects.filter(post=post)
        response['reviews'] = reviews
        user_has_reviewed = PostReview.objects.filter(user=request.user, post=post)
        response['user_has_reviewed'] = user_has_reviewed
    except:
        pass
    if (len(response) == 1):
        return redirect('catalog:katalog')
    return render(request, 'postdetails/postdetails.html', response)

def addPost(request):
    # seorang user dapat menambahkan post
    if (not request.user.is_authenticated):
        return redirect('sign_in')
    response = {'status_code':status_code_dict['NOTHING']}
    if (request.method == 'GET'):
        response['add_form'] = AddPostForm
        return render(request, 'postdetails/addpost.html', response)
    else:
        form = AddPostForm(request.POST, request.FILES)
        try:
            if (form.is_valid()):
                post = form.save(commit=False)
                post.user = request.user
                post.save()
                form.save_m2m()
                response['status_code'] = status_code_dict['POST_ADD_SUCCESS']
                response['post_id'] = post.id
        except:
            response['status_code'] = status_code_dict['POST_ADD_FAILED']
        return render(request, 'postdetails/redirector.html', response)
    
def deletePost(request, post_id):
    # seorang user dapat menghapus post yang dimilikinya
    if (not request.user.is_authenticated):
        return redirect('sign_in')
    response = {'status_code':status_code_dict['NOTHING'], 'post_id':post_id}
    if (request.method == 'POST'):
        try:
            post = Post.objects.filter(user=request.user, id=post_id)
            post.delete()
            response['status_code'] = status_code_dict['POST_DELETE_SUCCESS']
        except:
            response['status_code'] = status_code_dict['POST_DELETE_FAILED']
    return render(request, 'postdetails/redirector.html', response)

def addReview(request, post_id):
    # seorang user dapat menambah tepat satu ulasan pada sebuah post
    if (not request.user.is_authenticated):
        return redirect('sign_in')
    response = {'status_code':status_code_dict['NOTHING'], 'post_id':post_id}
    if (request.method == 'GET'):
        try:
            user_has_reviewed = PostReview.objects.filter(user=request.user, post__id=post_id)
            if (user_has_reviewed):
                response['status_code'] = status_code_dict['REVIEWED']
                return render(request, 'redirector.html', response)
            response['post'] = Post.objects.filter(id=post_id)[0]
            return render(request, 'postdetails/addreview.html', response)
        except:
            return redirect('catalog:katalog')
    else:
        form = ReviewPostForm(request.POST or None)
        try:
            if (form.is_valid()):
                post = Post.objects.filter(id=post_id)[0]
                review = form.save(commit=False)
                review.user = request.user
                review.post = post
                review.save()
                response['status_code'] = status_code_dict['REVIEW_ADD_SUCCESS']
        except:
            response['status_code'] = status_code_dict['REVIEW_ADD_FAILED']
        return render(request, 'postdetails/redirector.html', response)

def deleteReview(request, post_id):
    # seorang user dapat menghapus ulasan yang telah dia buat pada sebuah post
    if (not request.user.is_authenticated):
        return redirect('sign_in')
    response = {'status_code':status_code_dict['NOTHING'], 'post_id':post_id}
    if (request.method == 'POST'):
        try:
            review = PostReview.objects.filter(user=request.user, post__id = post_id)
            review.delete()
            response['status_code'] = status_code_dict['REVIEW_DELETE_SUCCESS']
        except:
            response['status_code'] = status_code_dict['REVIEW_DELETE_FAILED']
    return render(request, 'postdetails/redirector.html', response)
