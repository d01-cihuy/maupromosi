from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Post)
admin.site.register(PostCategory)
admin.site.register(PostLocation)
admin.site.register(PostReview)
